/**
 * TODO description
 */
import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;

import java.util.*;



public class UI extends JFrame {
	
	
	   ETextArea  buffer;
	   ETextAreaMiniBuffer miniBuffer;
       AbstractDocument doc;
       private static String newline = "\n";
       private static UI instance;
       
       public static UI getInstance(){
    	   if(instance == null){
    		   instance = new UI();
    	   }
    	   return instance;
       }
       public ETextArea  getETextAreaBuffer(){
    	   return buffer;
       }
       public ETextAreaMiniBuffer getETextAreaMiniBuffer(){
    	   return miniBuffer;
       }
       private UI(){
       	
    	super("EText");
	    buffer = new ETextArea();
	    
	    buffer.getBuffer().setCaretPosition(0);
	    buffer.getBuffer().setMargin(new Insets(5,5,5,5));
            StyledDocument styledDoc = buffer.getBuffer().getStyledDocument();
            if (styledDoc instanceof AbstractDocument) {
               doc = (AbstractDocument)styledDoc;
	        //doc.setDocumentFilter(new DocumentSizeFilter(MAX_CHARACTERS));
            } else {
              System.err.println("Text pane's document isn't an AbstractDocument!");
              System.exit(-1);
            }
            JScrollPane scrollPane = new JScrollPane(buffer.getBuffer());
            scrollPane.setPreferredSize(new Dimension(200, 200));

	    //Create the text area for the status log and configure it.
	    miniBuffer = new ETextAreaMiniBuffer();
	    
	    JScrollPane scrollPaneForLog = new JScrollPane(miniBuffer.getMiniBuffer());
	    //Create a split pane for the change log and the text area.
	    JSplitPane splitPane = new JSplitPane(
	    JSplitPane.VERTICAL_SPLIT,
	    scrollPane, scrollPaneForLog);
	    splitPane.setOneTouchExpandable(true);

	    //Create the status area.
	    JPanel statusPane = new JPanel(new GridLayout(1, 1));
	    CaretListenerLabel caretListenerLabel =
	    new CaretListenerLabel("Cursor Status");
	    statusPane.add(caretListenerLabel);

	    //Add the components.
	    getContentPane().add(splitPane, BorderLayout.CENTER);
	    getContentPane().add(statusPane, BorderLayout.PAGE_END);

	    //Add some key bindings.
	    buffer.addBindings();
	    miniBuffer.addBindings();
	    //Put the initial text into the text pane.
	    initDocument();
	    buffer.getBuffer().setCaretPosition(0);

	
       }
    //This listens for and reports caret movements.
    protected class CaretListenerLabel extends JLabel
                                       implements CaretListener {
        public CaretListenerLabel(String label) {
            super(label);
        }

        //Might not be invoked from the event dispatch thread.
        public void caretUpdate(CaretEvent e) {
            displaySelectionInfo(e.getDot(), e.getMark());
        }

        //This method can be invoked from any thread.  It 
        //invokes the setText and modelToView methods, which 
        //must run on the event dispatch thread. We use
        //invokeLater to schedule the code for execution
        //on the event dispatch thread.
        protected void displaySelectionInfo(final int dot,
                                            final int mark) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    if (dot == mark) {  // no selection
                        try {
                            Rectangle caretCoords = buffer.getBuffer().modelToView(dot);
                            //Convert it to view coordinates.
                            setText("caret: text position: " + dot
                                    + ", view location = ["
                                    + caretCoords.x + ", "
                                    + caretCoords.y + "]"
                                    + newline);
                        } catch (BadLocationException ble) {
                            setText("caret: text position: " + dot + newline);
                        }
                    } else if (dot < mark) {
                        setText("selection from: " + dot
                                + " to " + mark + newline);
                    } else {
                        setText("selection from: " + mark
                                + " to " + dot + newline);
                    }
                }
            });
        }
      // inherited constructors


       private CaretListenerLabel (  ) { super(); }
    }

      	    protected void initDocument() {
        String initString[] =
                {
                 
                  "Also to undo and redo changes.",
                  "Use the style menu to change the style of the text.",
                  "Use the arrow keys on the keyboard or these emacs key bindings to move the caret:",
                  "Ctrl-f, Ctrl-b, Ctrl-n, Ctrl-p." };

       // SimpleAttributeSet[] attrs = initAttributes(initString.length);

        try {
            for (int i = 0; i < initString.length; i ++) {
                doc.insertString(doc.getLength(), initString[i] + newline,
                       null); //attrs[i]);
            }
        } catch (BadLocationException ble) {
            System.err.println("Couldn't insert initial text.");
        }
    }
      	  protected SimpleAttributeSet[] initAttributes(int length) {
              //Hard-code some attributes.
              SimpleAttributeSet[] attrs = new SimpleAttributeSet[length];

              attrs[0] = new SimpleAttributeSet();
              StyleConstants.setFontFamily(attrs[0], "SansSerif");
              StyleConstants.setFontSize(attrs[0], 16);

              attrs[1] = new SimpleAttributeSet(attrs[0]);
              StyleConstants.setBold(attrs[1], true);

              attrs[2] = new SimpleAttributeSet(attrs[0]);
              StyleConstants.setItalic(attrs[2], true);

              attrs[3] = new SimpleAttributeSet(attrs[0]);
              StyleConstants.setFontSize(attrs[3], 20);

              attrs[4] = new SimpleAttributeSet(attrs[0]);
              StyleConstants.setFontSize(attrs[4], 12);

              attrs[5] = new SimpleAttributeSet(attrs[0]);
              StyleConstants.setForeground(attrs[5], Color.red);

              return attrs;
          }
    
	 /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
         UI frame = UI.getInstance();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    //The standard main method.
    public static void main(String[] args) {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //Turn off metal's use of bold fonts
	        UIManager.put("swing.boldMetal", Boolean.FALSE);
		createAndShowGUI();
            }
        });
    }
}