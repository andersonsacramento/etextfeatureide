/**
 * TODO description
 */
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import javax.swing.text.DefaultEditorKit;



public class ForwardWord implements Bind{
	public void bind(InputMap inputMap, ActionMap actionMap){
		//(Meta) Alt-f to go forward one word
		KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_F, Event.ALT_MASK);
		inputMap.put(key, DefaultEditorKit.nextWordAction);
	}
}