/**
 * TODO description
 */

import javax.swing.*;
import javax.swing.text.*;

import java.util.*;/**
 * TODO description

 */
import javax.swing.undo.UndoManager;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;/**
 * TODO description
 */
import javax.swing.undo.CannotRedoException;



abstract class ETextArea$$EText {
       JTextPane buffer = new JTextPane();
       //HashMap<Object, Action> actions;
       List binds;
       

       public void addBindings(){
    	     getBinds();
       	     InputMap inputMap = buffer.getInputMap();
       	     ActionMap actionMap = buffer.getActionMap();
       	     for(int i =0 ; i < binds.size(); i++){
	      	     ((Bind) binds.get(i)).bind(inputMap,actionMap);
	         }
       }
       public Document getDocument(){
    	   return buffer.getDocument();
       }
       public JTextPane getBuffer(){
       	      return buffer;
       }
       public void addBind(Bind bind){
    	   ((ETextArea) this).binds.add(bind);
       }
       public void getBinds(){
		binds = new ArrayList();
		binds.add(new ControlCommand());
		
	}
}



abstract class ETextArea$$Edit extends  ETextArea$$EText  {
	
protected UndoManager undoManager = new UndoManager();

 public ETextArea$$Edit(){
	 buffer.getDocument().addUndoableEditListener(
		        new UndoableEditListener() {
		            public void undoableEditHappened(UndoableEditEvent e) {
		              undoManager.addEdit(e.getEdit());
		            }
		          });
 }
 public UndoManager getUndoManager(){
	 return undoManager;
 }
}



abstract class ETextArea$$Undo extends  ETextArea$$Edit  {
	public void getBinds(){
		super.getBinds();
		binds.add(new Undo());
	}
	public void undo(){
		   try {
		          undoManager.undo();
		        } catch (CannotRedoException cre) {
		          cre.printStackTrace();
		   }
	}
      // inherited constructors



 public ETextArea$$Undo (  ) { super(); }
}



abstract class ETextArea$$Redo extends  ETextArea$$Undo  {
	public void getBinds(){
		super.getBinds();
		binds.add(new Redo());
	}
	public void redo(){
		 try {
	          undoManager.redo();
	        } catch (CannotRedoException cre) {
	          cre.printStackTrace();
	        }
	}
      // inherited constructors



 public ETextArea$$Redo (  ) { super(); }
}

/**
 * TODO description
 */
abstract class ETextArea$$MoveByCharacter extends  ETextArea$$Redo  {
	public void getBinds(){
		super.getBinds();
		binds.add(new BackwardCharacter());
		binds.add(new ForwardCharacter());
	}
      // inherited constructors



 public ETextArea$$MoveByCharacter (  ) { super(); }
}

/**
 * TODO description
 */
public class ETextArea extends  ETextArea$$MoveByCharacter  {
	public void getBinds(){
		super.getBinds();
		binds.add(new BackwardWord());
		binds.add(new ForwardWord());
	}
      // inherited constructors



 public ETextArea (  ) { super(); }
}