/**
 * TODO description
 */

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import javax.swing.text.DefaultEditorKit;



public class Redo implements Bind {
	
	public void bind(InputMap inputMap, ActionMap actionMap){
		//Shift-z to go backward one word
        KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.SHIFT_MASK);
        inputMap.put(key,"redo");
		actionMap.put("redo",  new AbstractAction() {
		    public void actionPerformed(ActionEvent e) {
		       UI.getInstance().getETextAreaBuffer().redo();
		    }
		}
		);
	}
}