/**
 * TODO description
 */
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import javax.swing.text.DefaultEditorKit;



public class BackwardCharacter implements Bind {
	public void bind(InputMap inputMap, ActionMap actionMap){
		//Ctrl-b to go backward one character
        KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_B, Event.CTRL_MASK);
        inputMap.put(key, DefaultEditorKit.backwardAction);

		
	}
}