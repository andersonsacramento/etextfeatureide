/**
 * TODO description
 */
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import javax.swing.text.DefaultEditorKit;



public class ForwardCharacter implements Bind{
	public void bind(InputMap inputMap, ActionMap actionMap){
		//Ctrl-f to go forward one character
		KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_F, Event.CTRL_MASK);
		inputMap.put(key, DefaultEditorKit.forwardAction);
	}
}