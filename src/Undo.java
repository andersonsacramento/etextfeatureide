/**
 * TODO description
 */

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import javax.swing.text.DefaultEditorKit;



public class Undo implements Bind {
	
	public void bind(InputMap inputMap, ActionMap actionMap){
		//(Control)Ctrl-z to go backward one word
        KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.CTRL_MASK);
        inputMap.put(key,"undo");
		actionMap.put("undo",  new AbstractAction() {
		    public void actionPerformed(ActionEvent e) {
		       UI.getInstance().getETextAreaBuffer().undo();
		    }
		}
		);
	}
}